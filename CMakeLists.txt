cmake_minimum_required(VERSION 2.8.3)
project(par)
 
add_definitions(-std=c++11 -Wall -Wextra -Wdeprecated)#-Wpedantic -Werror)

# Load catkin and all dependencies required for this package
find_package(catkin REQUIRED COMPONENTS 
  roscpp 
  
  message_generation
  std_msgs
  geometry_msgs
  
  actionlib
)

add_message_files(
   DIRECTORY msg
   FILES
   ik.msg
)


add_service_files(
   DIRECTORY srv
   FILES
   UR5eIK.srv
)

generate_messages(DEPENDENCIES roscpp std_msgs geometry_msgs)

catkin_package(
  CATKIN_DEPENDS 
    roscpp
    
    message_runtime
    geometry_msgs
    std_msgs
    
    actionlib
)

include_directories(${catkin_INCLUDE_DIRS})

add_executable(simple_subscriber src/simple_subscriber.cpp)
target_link_libraries(simple_subscriber ${catkin_LIBRARIES})

add_executable(simple_publisher src/simple_publisher.cpp)
target_link_libraries(simple_publisher ${catkin_LIBRARIES})

add_executable(simple_service_server src/simple_service_server.cpp)
target_link_libraries(simple_service_server ${catkin_LIBRARIES})
add_dependencies(simple_service_server ${catkin_EXPORTED_TARGETS} par_generate_messages_cpp)

add_executable(simple_service_client src/simple_service_client.cpp)
target_link_libraries(simple_service_client ${catkin_LIBRARIES})
add_dependencies(simple_service_client ${catkin_EXPORTED_TARGETS} par_generate_messages_cpp)

add_executable(simple_action_client src/simple_action_client.cpp)
target_link_libraries(simple_action_client ${catkin_LIBRARIES})

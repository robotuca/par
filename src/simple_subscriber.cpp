#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <iomanip> // for std::setprecision and std::fixed

// A callback function.  Executed each time a new pose
// message arrives.
void poseMessageReceived(const sensor_msgs::JointState& msg) {
	ROS_INFO_STREAM(std::setprecision(2) << std::fixed
	<< "q = (" <<  msg.position[0] << "," << msg.position[1] << "," << msg.position[2] << ")"
	<< msg.position[3] << "," << msg.position[4] << "," << msg.position[5]);
}


int main(int argc, char **argv) {
	// Initialize the ROS system and become a node.
	ros::init(argc, argv, "subscribe_to_pose");
	ros::NodeHandle nh;

	// Create a subscriber object.
	ros::Subscriber sub = nh.subscribe("/joint_states", 1000,
	&poseMessageReceived);

	// Let ROS take over.
	ros::spin();
}

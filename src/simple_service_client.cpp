#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <sstream>

#include <par/UR5eIK.h>


int main( int argc, char** argv )
{

    ros::init(argc, argv, "ik_client_node");
    ros::NodeHandle node;

    ROS_INFO("**** starting ik client node ****");

    ros::service::waitForService("/UR5eIK");
    ros::ServiceClient ur5eik_client = node.serviceClient<par::UR5eIK>("/UR5eIK");
    par::UR5eIK ur5eik_srv;
      
    ur5eik_srv.request.frontrear = 1;
    ur5eik_srv.request.elbow = -1;
    ur5eik_srv.request.wrist = -1;

    ur5eik_srv.request.pose.position.x = 0.422;
    ur5eik_srv.request.pose.position.y = -0.039;
    ur5eik_srv.request.pose.position.z = 0.100;
    ur5eik_srv.request.pose.orientation.x = 0.707;
    ur5eik_srv.request.pose.orientation.y = -0.707;
    ur5eik_srv.request.pose.orientation.z = 0.0;
    ur5eik_srv.request.pose.orientation.w = 0.0;
        
    
    ROS_INFO_STREAM("Robot Pose: [" << 
        ur5eik_srv.request.pose.position.x << ", " <<
        ur5eik_srv.request.pose.position.y << ", " <<
        ur5eik_srv.request.pose.position.z << ", " << 
        ur5eik_srv.request.pose.orientation.x << ", " << 
        ur5eik_srv.request.pose.orientation.y << ", " <<
        ur5eik_srv.request.pose.orientation.z << ", " <<
        ur5eik_srv.request.pose.orientation.w << "]");
    
    ur5eik_client.call(ur5eik_srv);
    
    std::stringstream sstr;
    if(ur5eik_srv.response.status)
    {
        sstr<<"The computed ik is:"<<std::endl;
        for(int i=0; i<ur5eik_srv.response.ik_solution.size(); i++)
        {
            sstr << "[";
            for(int j=0; j<5; j++)
            {
                sstr << ur5eik_srv.response.ik_solution[i].ik[j] <<", ";            
            }
            sstr << ur5eik_srv.response.ik_solution[i].ik[5] << "]" << std::endl;
        }
        ROS_INFO_STREAM(sstr.str());
    }
    else{
        ROS_INFO("Not able to compute the ik");
    }
     
}

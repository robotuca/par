#include <ros/ros.h>
#include <trajectory_msgs/JointTrajectory.h>  // For trajectory_msgs::JointTrajectory
#include <stdlib.h>

int main(int argc, char **argv) {
	// Initialize the ROS system and become a node.
	ros::init(argc, argv, "publish_pose");
	ros::NodeHandle nh;
	ros::Rate loop_rate(50);
	
	// Create a publisher object.
	ros::Publisher pub = nh.advertise<trajectory_msgs::JointTrajectory>(
	"pos_joint_traj_controller/command", 5);
	
	
	std::vector<double> q_target(6);
	double t = 3.0;
	nh.getParam("simple_publisher/q1", q_target[0]);
	nh.getParam("simple_publisher/q2", q_target[1]);
	nh.getParam("simple_publisher/q3", q_target[2]);
	nh.getParam("simple_publisher/q4", q_target[3]);
	nh.getParam("simple_publisher/q5", q_target[4]);
	nh.getParam("simple_publisher/q6", q_target[5]);
	nh.getParam("simple_publisher/t", t);

	ROS_INFO_STREAM("Target position:"
		<< " q_t = " 
		<< q_target[0] << ", "
		<< q_target[1] << ", "
		<< q_target[2] << ", "
		<< q_target[3] << ", "
		<< q_target[4] << ", "
		<< q_target[5]);



	while (0 == pub.getNumSubscribers()) 
	{
		ROS_INFO("Waiting for subscribers to connect");
		ros::Duration(0.001).sleep();
	}
	
//	while(ros::ok()) {		
		trajectory_msgs::JointTrajectory msg;
		trajectory_msgs::JointTrajectoryPoint points;

		msg.header.stamp = ros::Time::now();
		msg.header.frame_id = "base_link";
		
		msg.joint_names.resize(6);
		msg.joint_names[0] ="shoulder_pan_joint";
		msg.joint_names[1] ="shoulder_lift_joint";
		msg.joint_names[2] ="elbow_joint";
		msg.joint_names[3] ="wrist_1_joint";
		msg.joint_names[4] ="wrist_2_joint";
		msg.joint_names[5] ="wrist_3_joint";
		
		msg.points.resize(1);
		msg.points[0].positions.resize(6);
		msg.points[0].positions[0] = q_target[0];
		msg.points[0].positions[1] = q_target[1];
		msg.points[0].positions[2] = q_target[2];
		msg.points[0].positions[3] = q_target[3];
		msg.points[0].positions[4] = q_target[4];
		msg.points[0].positions[5] = q_target[5];	
		
		msg.points[0].time_from_start.sec = t;
		msg.points[0].time_from_start.nsec = 0;
		
		// Publish the message.
		pub.publish(msg);

		// Send a message to rosout with the details.
		ROS_INFO_STREAM("Sending q_t:"
		<< " q_t = " 
		<< msg.points[0].positions[0] << ", "
		<< msg.points[0].positions[1] << ", "
		<< msg.points[0].positions[2] << ", "
		<< msg.points[0].positions[3] << ", "
		<< msg.points[0].positions[4] << ", "
		<< msg.points[0].positions[5]);

		ros::spinOnce();

		// Wait until it's time for another iteration.
		loop_rate.sleep();
//	}

}


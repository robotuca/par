#include "ros/ros.h"

#include "par/UR5eIK.h"




bool srvURIK(par::UR5eIK::Request &req, par::UR5eIK::Response &res)
{

    int frontrear = req.frontrear;
    int elbow	   = req.elbow;
    int wrist	   = req.wrist;

    if (frontrear == 1)
        ROS_INFO("Solucion frontal.\n");
    else if (frontrear == -1)
        ROS_INFO("Solucion posterior.\n");
    else
    {
        ROS_INFO("Parametro frontrear erroneo.\n");
        res.status = false;
        return false;
    }

    if (elbow == 1)
        ROS_INFO("Solucion codo arriba.\n");
    else if (elbow == -1)
        ROS_INFO("Solucion codo abajo.\n");
    else
    {
        ROS_INFO("Parámetro elbow erroneo.\n");
        res.status = false;
        return false;
    }

    if (wrist == 1)
        ROS_INFO("Solucion muneca arriba.\n");
    else if (wrist == -1)
        ROS_INFO("Solucion muneca abajo.\n");
    else
    {
        ROS_INFO("Parametro wrist erroneo.\n");
        res.status = false;
        return false;
    }
    
    double q[6] = {1,2,3,4,5,6};
    res.ik_solution.resize(1);
    
    for (int i=0;i<6;i++)
        res.ik_solution[0].ik.push_back(q[i]);
    
    res.status = true;
    return true;
    
}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "UR5eIK_node");
    ros::NodeHandle n;

    ROS_INFO("Starting UR5e inverse kinematic service");

    ros::ServiceServer service1 = n.advertiseService("/UR5eIK", srvURIK);
    ros::spin();

    return 0;
}

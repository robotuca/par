#include <ros/ros.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <sensor_msgs/JointState.h>


typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> Client;

control_msgs::FollowJointTrajectoryGoal goal;
actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> *robotClient;


int numjoints = 6;
std::vector<std::string> jointnames;
bool trajectoryset = false;
std::vector<double> currentjoints;

bool b_init_q = false;

//Callback function: Called once when the goal completes
void doneCb(const actionlib::SimpleClientGoalState& state,
            const control_msgs::FollowJointTrajectoryResultConstPtr& result)
{
  ROS_INFO("Finished in state [%s]", state.toString().c_str());
  ROS_INFO("Answer: error_code is %d", result->error_code);
}

//Callback function: Called once when the goal becomes active
void activeCb()
{
  ROS_INFO("Goal just went active");
}

//Callback function: Called every time feedback is received for the goal
void feedbackCb(const control_msgs::FollowJointTrajectoryFeedbackConstPtr& feedback)
{
  std::stringstream feedback_message_names;
  feedback_message_names << "Got Feedback of joints (";
  for(int i=0;i<numjoints-1;i++)
      feedback_message_names << feedback->joint_names[i] << ",";
  feedback_message_names << feedback->joint_names[numjoints-1] << ")";
  ROS_INFO_STREAM(feedback_message_names.str());

  std::stringstream feedback_message_pos;
  feedback_message_pos << "              current positions are (";
  for(int i=0;i<numjoints-1;i++)
      feedback_message_pos << feedback->actual.positions[i] << ",";
  feedback_message_pos << feedback->actual.positions[numjoints-1] << ")";
  ROS_INFO_STREAM(feedback_message_pos.str());

  std::stringstream feedback_message_vel;
  feedback_message_vel << "              current velocities are (";
  for(int i=0;i<numjoints-1;i++)
      feedback_message_vel << feedback->actual.velocities[i] << ",";
  feedback_message_vel << feedback->actual.velocities[numjoints-1] << ")";
  ROS_INFO_STREAM(feedback_message_vel.str());
}


//Function to send the goal to the FollowJointTrajectory action server.
//Waits for the result for trajduration seconds.
//If not able to reach the goal within timeout, it is cancelled
bool moveRobotTrajectory(double trajduration)
{
    ROS_INFO("Moving robot ");

    if(!trajectoryset)
    {
       ROS_ERROR(" *** Trajectory not yet set. Please call first the set_trajectory or set_rectilinear_trajectory service *** ");
       return false;
    };

    //Print the trajectory to be followed
    ROS_INFO("currentjoints: (%f,%f,%f,%f,%f,%f)",
                  currentjoints[0],
                  currentjoints[1],
                  currentjoints[2],
                  currentjoints[3],
                  currentjoints[4],
                  currentjoints[5]);
    for(int i=0; i < goal.trajectory.points.size(); i++){
         ROS_INFO("%d: (%f,%f,%f,%f,%f,%f)",i,
                  goal.trajectory.points[i].positions[0],
                  goal.trajectory.points[i].positions[1],
                  goal.trajectory.points[i].positions[2],
                  goal.trajectory.points[i].positions[3],
                  goal.trajectory.points[i].positions[4],
                  goal.trajectory.points[i].positions[5]);
    }

    //Set timestamp and send goal
    goal.trajectory.header.stamp = ros::Time::now();// + ros::Duration(1.0); //To cHECK the +1
    robotClient->sendGoal(goal, &doneCb, &activeCb, &feedbackCb);

    //Wait for the action to return. Timeout set to the req.trajduration plus the goal time tolerance)
    bool finished_before_timeout = robotClient->waitForResult(ros::Duration(trajduration) + goal.goal_time_tolerance);

    //Get final state
    actionlib::SimpleClientGoalState state = robotClient->getState();
    if (finished_before_timeout) {
        //Reports ABORTED if finished but goal not reached. Cause shown in error_code in doneCb callback
        //Reports SUCCEEDED if finished and goal reached
        ROS_INFO(" ***************** Robot action finished: %s  *****************",state.toString().c_str());
    } else {
        //Reports ACTIVE if not reached within timeout. Goal must be cancelled if we want to stop the motion, then the state will report PREEMPTED.
        ROS_ERROR("Robot action did not finish before the timeout: %s",
                state.toString().c_str());
        //Preempting task
        ROS_ERROR("I am going to preempt the task...");
        robotClient->cancelGoal();
    }
    //force to reset the traj before calling again move
    trajectoryset = false;
}

//Function to set a rectilinear trajectory
// deltaconf is used to define the goal: goal=currentjoints+deltaconf
// epsilon is the step size in joint space
// timefromstart defines the total trajectory time
void setRectilinearTrajectory(std::vector<double>& deltaconf,double epsilon, double timefromstart)
{
        ROS_INFO("Setting new rectilinear trajectory ");

        //set joint names
        goal.trajectory.joint_names.resize(6);
        goal.trajectory.joint_names[0] = "elbow_joint";
        goal.trajectory.joint_names[1] = "shoulder_lift_joint";
        goal.trajectory.joint_names[2] = "shoulder_pan_joint";
        goal.trajectory.joint_names[3] = "wrist_1_joint";
        goal.trajectory.joint_names[4] = "wrist_2_joint";
        goal.trajectory.joint_names[5] = "wrist_3_joint";

        goal.trajectory.points.resize( 1 );
        goal.trajectory.points[0].positions.resize( numjoints );
        for(int j=0; j < numjoints; j++)
            goal.trajectory.points[0].positions[j] = currentjoints[j] + deltaconf[j];
                
        goal.trajectory.points[0].time_from_start = ros::Duration(timefromstart);
        trajectoryset = true;
}


// A callback function to update the arm joint values - gripper joint is skipped
void updateCurrentJointStates(const sensor_msgs::JointState& msg) {
  for(int i=0; i<msg.name.size();i++)
  {
    //BE CAREFUL: order of joints is set according to JointStates
    if(msg.name[i] == "elbow_joint") currentjoints[0]= msg.position[i];
    else if(msg.name[i] == "shoulder_lift_joint") currentjoints[1]= msg.position[i];
    else if(msg.name[i] == "shoulder_pan_joint") currentjoints[2]= msg.position[i];
    else if(msg.name[i] == "wrist_1_joint") currentjoints[3]= msg.position[i];
    else if(msg.name[i] == "wrist_2_joint") currentjoints[4]= msg.position[i];
    else if(msg.name[i] == "wrist_3_joint") currentjoints[5]= msg.position[i];

    
    //std::stringstream ss;
    //for(int j=0; j<6;j++)
    //  ss<<currentjoints[j]<<" ";
    //ROS_INFO_STREAM(ss.str());
    
  }
  b_init_q = true;
}


int main(int argc, char **argv)
{
  // Initialize the ROS system and become a node.
  ros::init(argc, argv, "par_action_client");
  ros::NodeHandle nh;

  // Create a subscriber object.
  ros::Subscriber sub = nh.subscribe("joint_states", 1000, &updateCurrentJointStates);

  // Initialization
  goal.goal_time_tolerance = ros::Duration(1);
  currentjoints.resize(numjoints);
  robotClient = new Client("pos_joint_traj_controller/follow_joint_trajectory");

  // Wait for user to press a key
  std::cout<<"\nMOVE THE ROBOT TO A SAVE HOME CONFIGURATION AND PRESS A KEY TO START..."<<std::endl;
  std::vector<double> deltaconf;
  deltaconf.resize(6);
  int signe = -1;
  double epsilon =  0.1;
  double trajduration = 5;

  while(ros::ok()) {
    ros::spinOnce();
    if (b_init_q){
    //Wait for user to press a key
    std::cout<<"\nPRESS a key to start a rectilinear joint-space test motion" << std::endl;
    std::cin.get();
    if(signe == -1) signe = 1;
    else signe = -1;
    deltaconf[0] =  signe*0.3;//elbow
    deltaconf[1] =  signe*0.0;
    deltaconf[2] =  signe*0.3;//pan
    deltaconf[3] =  signe*0.0;
    deltaconf[4] =  signe*0.0;
    deltaconf[5] =  signe*0.0;
    setRectilinearTrajectory(deltaconf, epsilon, trajduration);
    moveRobotTrajectory(trajduration);
    }
  }

}
